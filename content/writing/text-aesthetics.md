---
title: "Text Aesthetics"
date: 2019-07-09T00:00:00+01:00
---
# Text Aesthetics

<div><img src="/images/text-aesthetics.jpg" style="border-radius: 4px; max-width: 100%;"></div>

### Everything that can be read, is seen first.

When writing, it's not all about the content, have you also considered,
*the aesthetics?*

It is the way your writing looks, whether written on paper, in print, even on
websites; *Text aesthetics* impact how your words are read - consider that
people are judged for how they look, the same is true of writing.

---

#### This article shows you the principles that will put your writing in it's best light.

Let's begin.

#### 1 - Contrast

Your text stands out when you make it *look* different from is neighbours. You'll already be familiar with the tools available here:
You can *italicise* or **embolden**, even changing <font size="12">up</font> the font size. A <span style="color: hsl(0, 0%, 50%)">change</span> of color..

There are more ways to contrast text than meets the eye, the key is to be *deliberate* about it.

#### 2 - Concision

Yes Ladies and Gentlemen, less can indeed be more. The fewer words used, the
clearer your message becomes. This makes it appealling, which is why
copywriters have a short headline to draw you in.

A *hook*.

Why is it called a hook? It's a line containing something small enough that it looks easily digestable.. only to draw you in and read the rest of the ad.

Be like a copywriter and leave us a word snack.

#### 3 - Suitability

The aesthetics of your text should match your content. A good example of
suitability is franco manca's logotype (type used in their logo)

<div><img src="/images/franco-manca-logo.svg"></div>

Franco Manca have a font that reminds your of pizza dough, The shape looks homemade too, giving off a friendly touch. This is exactly what you want outside a restaurant.

#### 4 - Readability

This may sound a bit weird, but you must be able to easily read the words you are presenting.

Experiment with a slightly bigger line height for paragraphs and perhaps slightly narrower letting for headers.

#### 5 - Hierarchy

Reader's expect structure in a text, so make it clear for them as a way of conforming to the more [general principle of least astonishment](https://en.wikipedia.org/wiki/Principle_of_least_astonishment).

Headers are the go to way of indicating hierarchy but many of the examples in
the first section on Contrast could be used too.

#### 5 - Italics are a different type

Italics are not just slanted versions of a regular type, there are commonly
distinctions between them so you should look to provide the italic font you want.  

Ensure you have the italic type if you are going to use it, the computed version
from the regular font will *not* look right.

### In Summary

Writing isn't just for reading, there is a visual aspect that is influential on your readers. This is just an introduction in to some of what I've learnt recently - if you found it useful I'd love to hear about it on [twitter](https://twitter.com/mikepjb)

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/writing/text-aesthetics/" />
<meta property="og:title" content="Text Aesthetics" />
<meta property="og:image" content="https://michaelbruce.co/images/text-aesthetics.jpg" />
