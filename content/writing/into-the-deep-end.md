---
title: Into the deep end - full heading
date: 2019-11-04
publishdate: 2019-11-04
---

Fully immerse yourself in what you do. Commit to achieving something. No matter the task, it gets easier once you dive in the deep end.

In a paradoxical way, committing to a task makes it easier to stay committed. The more you focus, the more you know about a subject. The more information you have to take interest in.

Why is this important? Whatever you choose to do should be something worth doing well. Time is finite, you have a lot less of it than you'd like.

Not every moment can be optimally spent. You need to relax too, getting rest is important. Committing fully to this is important too.

Checking your phone in a meeting, stops you focusing on the subject at hand. The same applies when you take your phone with you to bed, you're not getting the rest that you need.

Phones are such an easy but overlooked example, because they are part of everyday life. By design they are carried around and always accessible.

Technology does not always provide an upgrade, as in the examples above it can rob you of simple luxuries.

When you focus on a given task, when you can isolate yourself from distractions you'll be surprised at what you can achieve.

Many distractions come so often that they are considered unavoidable, I still feel like it can be good to be distracted at some points in the day. Being open to conversation and spotnaneous events gives you back something that you can't get by focusing on your own tasks.

Focusing on tasks takes energy too, we have a certain amount of effort we can use to focus each day. Reducing distractions by preparing for them in a useful strategy.

For example, you have to get dressed everyday. Having the same set of clothes to wear everyday is not normal but it's what Steve Jobs did. Having an organised wardrobe avoids you having to think about getting ready, you can instead focus on tasks that you can't prepare for in the same way.

The same goes for food, I have a spreadsheet that plans out what to buy for my family. The investment means that I have food in the house ready, I know what meals can be made. It's delicious and energy giving.

These are all different example of the same thing.. --
