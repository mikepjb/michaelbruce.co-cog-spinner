---
title: Discussion shows you a new world of understanding
date: 2019-10-28
publishdate: 2019-10-28
---

# Discussion shows you a new world of understanding

<div><img src="/images/argument.jpg"></div>

There are many worlds. Many places that we exist. In my mind I think this. In your mind you think differently.

On earth there are many realities. Some think eating meat is evil, others think it is necessary. I think it is very healthy to consume meat but not necessary.

In a world with so many worlds, it comes as no surpise that we disagree. Disagreement is good because whilst is appears as though there many worlds, truth is we are living in one.

If you aren't used to argument, discussing the pros and cons of a subject then you might think arguments are always bad. It is common to assume the worst in others and use slurs but that is missing the point entirely.

Discussion is necessary for us to learn. We are biased in our own interests. It is less likely that you will freely admit wrongdoing by someone who is the same sex or tribe as you. An outside opinion can shed light in such situations.

I've been lucky enough to run the Discussion Society at University. There is a lot of benefit, and enjoyment to had joining the discourse.

A favourite evening was inviting the Islamic Society to discuss Draw Muhammad Day. It was a great opportunity to talk with other humans about this divisive issue. Hearing many points of view has the crystallising effect of making the underlying issues clear. Do you have the right to make fun of religion? Is any organisation or person above satire?

Now we didn't walk away thinking the same way. That isn't the point. We did walk away at least knowing the arguments against our own positions. This is more important.

When ideas are scrutinised, they become stronger. Ideas that are challenged by examples described from another point of view ensures that is becomes more objective.

On Twitter, I make sure to follow people that have different opinions to me. Vegans are a good example (I used to be one), you will see lots of polarising speech about other tribes. It is better to see for yourself, we are all human and not so different.

It is common to see strawmen, unrealistic portrayals of the other side of an argument. Every group will have a subgroup that see the other side as ridiculous.

Some people take enjoyment out of this and add fuel to the fire by trolling. Trolling is helpful, I see it as a critical thinking exercise to discern whether a source of information is genuine.

There is such a wide variety of information now. Hyperbolic media that make more money with more polarisation. Science< Journals that get backed by lobby groups. 

Everyone and every group has a bias. Discussing is a lot of fun, I would have it no other way.

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/writing/a-new-world-of-understanding/" />
<meta property="og:title" content="Discussion shows you a new world of understanding" />
<meta property="og:image" content="https://michaelbruce.co/images/argument.jpg" />
