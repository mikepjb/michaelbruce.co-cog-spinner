---
title: "Why is everyone offering me cookies?"
date: 2018-10-21T00:00:00+01:00
---

# Why is everyone offering me cookies?

<div><img src="/images/cookies.jpg"></div>

<span class="a-drop-cap">A</span><span>lmost every site today will have a button asking you to accept cookies.
This is thanks to a change in law from the EU requiring sites to get explicit permission from visitors that they accept them.</span>

## What are cookies, and why do companies want you to have theirs?

Allow me to explain,

Cookies are small files that a website is able to store on your computer to
control the experience of your visit there, being able to store information
about you on your own computer is very useful.

- It can store the information you entered into a form, incase you lose internet
  connection or close a browser window.
- It can hold the items in your shopping cart on sites.
- It can be used to build a profile for targeted advertising.

This should make it clear why some sites choose to use cookies, simply because
they are useful.

## Why am I being asked to accept them?

Because sometimes they can be used to invade your privacy, to protect consumers
there was an EU directive that is now law in many countries. The aim is to give
the consumer the power to say whether or not they want to be tracked and have
their personal information collected.

## Not all cookies are created equal

Some cookies are exempt from the law, storing information about whether you are
logged in and what items you have in a shopping cart are considered essential
and are exempt.

Tracking which sites you've been to need explicit permission from the user,
Facebook is notable for doing this where people would include clickable buttons
to their site which essetially gave a backdoor to writing their own cookies on
where you've been.

## Should you accept cookies?

Yes, there is rarely any harm is doing so. It is just a shame that it takes away
focus from the visitor to the site, essentially it is a call to action that has
no direct business purpose.

It's likely as much an annoyance to the site owner as it is to you the
visitor. They'd rather not have to tell you about something so insignificant,
when they want to draw your attention to their own products.

It is done now because it is the law and companies don't want to be penalised.

## What does this site do?

This site uses a single cookie, `cfduid` from my chosen content delivery network
Cloudflare. It is used to apply specific security settings on individual
computers in a shared network e.g Starbucks WiFi. It is exempt on the grounds it
is necessary for public security.

## In Summary

Cookies are useful, take one by all means, it's a shame companies are compelled
by law to make explicit something that is often insignificant to the visitor
experience. At the same time it is a good thing that we have a law that can be
used to penalise companies that abuse your privacy.

Recently I've seen an uptick in sites that only use essential cookies. This is
the way forward, it is leading to the end user not having to worry about cookies
with effective law in place to prevent cookie misuse.

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url" content="https://michaelbruce.co/2018-10-21-why-is-everyone-offering-me-cookies" />
<meta property="og:title" content="Why is everyone offering me cookies?" />
<meta property="og:image" content="https://michaelbruce.co/images/cookies.jpg" />
