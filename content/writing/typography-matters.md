---
title: "Typography Matters"
date: 2018-10-25T00:00:00+01:00
---
# Typography Matters

<div><img src="/images/neon-sign.jpg"></div>

<span class="w-drop-cap">W</span><span>ritten word is our most commonly used tool. In every corner of the
world we communicate with each other this way.</span>

**Typography** is how words are presented and it has a big impact on how your message
is received.

It is a major element under consideration when designing a site and is something
that is often overlooked. The sheer volume of words we read everyday can drown
out the small details that are worth taking a second look at. It is common place
to quite literally gloss over them as you scan blocks of text.

Below I will explain a bit about different type aspects and how they effect the
delivery of your content.

## Styling

A major aspect to consider is the styling of the type you choose. Styles of type
are generally divided into 4 major categories.

- Serif
- Sans serif
- Script
- Monospace

If you want to have a serious tone about the subject under discussion, you might
choose to use a **serif type**. The additional decoration of each character with
a serif convey a sense of tradition and elegance, if that's what you want to
associate with your work then it's worth taking a closer look at this category.

<div class="large-serif-example blockquote">
Their presentation was acknowledged.
</div>

Serifs are more traditional, having their accents originating from
painting marks in stone carvings setting the position that the particular letter
should be cut.

**Monospaced** types are very mechanical in character and may put the reader in
mind of an old typewriter or a computer terminal. Typically monospace is used to
contrast between blocks of descriptive text and code examples on technical
blogs.

<div class="large-monospaced-example blockquote">
Their presentation was acknowledged.
</div>

**San-serifs** on the other hand omit the little accents or serifs entirely,
presenting a much cleaner appearance that can be both bold and friendly.
San-serif types can stand on their own two feet and will normally serve as the
foundation to your work.

San-serifs are often chosen for the main body of sites today and are very
legible which will help especially if you are targeting an audience that is
learning English. If you cannot think of any particular reason to use a
different face, this is a good type to default to.

<div class="large-sans-serif-example blockquote">
Their presentation was acknowledged.
</div>

Lastly, **script** types are those that convey a personal touch and for this
reason end up being the most varied of any major type. If you want your message
to completely stand out from anything else your audience will have seen, then
this category is worth considering. Rough handwritten types, highly stylised
calligraphy & medieval manuscript lettering all belong here. Because you are
spoilt for choice it's somewhat hard to generalise beyond what has already been
said.

<div class="large-script-example blockquote">
Their presentation was acknowledged.
</div>

These are macro groups of course, one serif type will look significantly
different to another and it's up to the designer to make a decision as to what
particular style they want to go with. There are many subdivisions and flavours
to choose from.

**This site uses Montserrat**, a sans-serif type chosen for the consistent
shapes in each letter. For example the letter o has a similar curved shape to
the main body of the g character. This is quite pleasing on the eye and very
readable. When reading content publish here I want to convey an easiness and
comfort when you read it.

## Contrasting between types

Types that are stylistically different from other elements on the page will
stand out more, if you've chosen a serif for most of the content in your work
you might consider going with something else for the header.

## Spacing & Size

The spacing between individual letters and lines affects how legible your
writing is. It's a good idea to play around to find a sweet spot that works, you
might find a particular large type heading reads better with each letter closer
together but as a text it does not.

<div class="tight-header-example blockquote">
Welcome.
</div>

<div class="wider-text-example blockquote">
Their presentation was acknowledged.
</div>

Line spacing too is worth experimenting with, this site is using a line height
of 1.4. Usually it's a good idea to give your lines a bit more breathing room
than the default I find.

When it comes to size, generally the bigger the better, make it easy for your
audience to read your words. Giving your text plenty of breathing room between
other elements is a great move too.

## In Summary

This is only a basic introduction, typography is a very deep and extensive are
of study. It's also an extremely useful ally for amplifying your message.

These are the key take home points:

- Sans-serif are your bread and butter, use them for the main bulk of your
  content.
- Serifs work well as headers but also for content if you are going for
  a more established and traditional feeling in your work.
- Monospace is used for technical examples.
- Script gives your content an individuality and personal touch.

Hopefully you'll find the process of experimenting on types as fun as I do.

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/2018-10-25-typography-matters" />
<meta property="og:title" content="Typography Matters" />
<meta property="og:image" content="https://michaelbruce.co/images/neon-sign.jpg" />
