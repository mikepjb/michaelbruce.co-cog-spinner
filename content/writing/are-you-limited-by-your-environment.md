---
title: Are you limited by your environment?
date: 2019-11-12
publishdate: 2019-11-12
---

Often I get to write when I'm on the train. I'm thankful for this, it's good to have a rhythm to daily life. How does the same environment affect your writing?

What do you mean by your environment? Is the exterior that important? Does dressing in nice clothes or feeding yourself well make for better experience than the commutting company you keep?

There are, as it turns out, quite a few people writing with me today.

Once you start writing and wondering, do your words take you elsewhere? Is your environement static? How much of it can you change? What are the limitations?

Getting a little bit meta here, writing this has got me thinking this: You are your own environment.

The same room can feel so different, what changed? How is moving into a flat for the first time and feeling at hom
