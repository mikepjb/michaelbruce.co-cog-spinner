---
title: "Site Anatomy 101"
date: 2018-11-04T00:00:00+01:00
---
# Site Anatomy 101

<div><img src="/images/site-anatomy-101.png"></div>

<span class="i-drop-cap">I</span><span>f you've ever wanted to know what a site
is made of, you came to the right place. I've put together a simple site that
takes a dataset on food composition and presents a graph to the
user.</span>

Before we begin, you may want to **see the example** in action <a
href="/nutrient-composition-1">here</a>.

Seen it? If all went well, you should have been presented with a graph much like
the one above listing the top 50 foods by the amount of protein contained in
grams.

## So what happened?

By **clicking the link** you made a web request, a specific kind of web request
known as a **GET request**.

That request was sent to my site, [michaebruce.co](michaebruce.co) which contains the following
source code:

```
mux.HandleFunc("/nutrient-composition-1", func(w http.ResponseWriter, r *http.Request) {
  nutrient.Graph(w)
})
```

This is known as a **route** because it handles web requests that the server
receives and passes on to other parts of the system.

This particular route is calling the **method** Graph, which is contained inside
a package, aptly named nutrient.

## Let's draw a map of the journey so far

If you're new to this side of websites, all this terminology may not make a
whole lot of sense so let's model what's been covered and what will be covered
later on in the article.

<div class="map-container">

<div class="map-level">
<div class="map-user map-element">
You, the user
<div class="map-desc">I'll let you fill in this description about yourself.</div>
<div>
<div class="map-path-red">&nbsp;</div>
</div>
</div>
</div>

<div class="map-level">
<div class="map-element">
You viewed a graph
<div class="map-desc">You clicked a link that made a web request. (this is what all links do!)</div>
<div><div class="map-path-red">&nbsp;</div></div>
</div>
</div>

<div class="map-level">
<div class="map-element mono">
GET /nutrient-composition-1
<div class="map-desc">This is the route that matched your web request, it
passes this message to nutrient.Graph.</div>
<div><div class="map-path-red">&nbsp;</div></div>
</div>
</div>

<div class="map-level">
<div class="map-element mono">
nutrient.Graph
<div class="map-desc">This is the method that is being called as a result of the
web request, which is what we'll look at next.</div>
<div><div class="map-path-red">&nbsp;</div></div>
</div>
</div>

<div class="map-level">
<div class="map-element mono">
nutrient. readNutrients
<div class="map-desc">This is going to read from a CSV dataset and pass some
data to prepareChart</div>
<div><div class="map-path-red">&nbsp;</div></div>
</div>

<div class="map-element mono">
nutrient. prepareChart
<div class="map-desc">Takes data from readNutrients to create the graph</div>
<div><div class="map-path-red">&nbsp;</div></div>
</div>
</div>

<div class="map-level">
<div class="map-element map-db">
CSV Dataset
<div class="map-desc">
A large collection of data on food composition.
</div>
<div>
<div class="map-path-red">&nbsp;</div>
</div>
</div>
</div>

<div class="legend">
Legend:
<div><span class="map-user sample">&nbsp;</span> User (Layer 0)</div>
<div><span class="map-view sample">&nbsp;</span> View (Layer 1)</div>
<div><span class="mono sample">&nbsp;</span> Action (Layer 2)</div>

<div><span class="map-path-red">&nbsp;</span>Red Storyline</div>
</div>
</div>

## Drawing the graph

Back to the code, let's have a look at the implementation of the Graph method
that is defined in our package `nutrient`:

```
func Graph(w http.ResponseWriter) {
	foods := readNutrients()
	graph := prepareChart(foods)

	err := graph.Render(chart.PNG, w)

	if err != nil {
		log.Fatal("bad graphing: %v", err)
	}
}
```

We've covered a lot of concepts already in this article, so I'll summarise the
important things that are happening here.

- **w** is a *parameter*, that is something that is passed as an argument to the
  method. In this case, it must be of the type http.ResponseWriter - in laymans
  terms this is saying that when you call `Graph` you should provide something
  that can write a response to a web request.
- **readNutrients()** is using a csv dataset to collect information that will be
  passed to our graphing function.
- **prepareChart** is the graphing function, that takes the output of
readNutrients, foods.
- **graph.Render** is creating an image file, this is passed to **w**, our
  `ResponseWriter` that sends the graph image to your web browser!

This completes our journey from request to result.

## In Summary

With just a few parts, you can do some pretty amazing things. The above
site takes no user input but is a great example of a bare bones site. It takes a
request, performs an activity and returns the result to your webpage.

These three elements:

  - **receiving requests**
  - performing an activity, **preparing a response**
  - **sending back the response** to be viewed in a web browser.

Are the foundation upon which every site on the internet functions.

## Thanks for reading

I hope you enjoyed reading this, my plan is to expand on this example and
include some user input so you can get an idea for how that works too.

I take a lot of enjoyment pulling apart life’s complex problems, particularly
when it comes to technology. If you liked this article, perhaps you’d like my
help building your next site or system. You can get started by using the form
below.

Until next time!

# References

[McCance and Widdowson’s 'composition of foods integrated dataset' on the nutrient content of the UK food supply. ](https://www.gov.uk/government/publications/composition-of-foods-integrated-dataset-cofid)

This site's source code:  

[The server file that contains the /nutrient-composition-1 route definition](https://bitbucket.org/mikepjb/michaelbruce.co/src/master/server.go#lines-98)

[The nutrient component which creates the graph](https://bitbucket.org/mikepjb/michaelbruce.co/src/434165a7f27554b9ad7365d22c38bf41b9f66cde/src/nutrient/nutrient.go#lines-126)

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/2018-11-04-site-anatomy-101" />
<meta property="og:title" content="Site Anatomy 101" />
<meta property="og:image" content="https://michaelbruce.co/images/site-anatomy-101.png" />
