---
title: Fear
date: 2019-10-31
publishdate: 2019-10-31
---

It's Halloween today so I want to talk about fear.

Fear is the ultimate challenge, no where else will you find a more even match. It is a battle with yourself, for yourself.

Fear is with you all the time, when you hesitate, avoid embarassment, you are so used to the engagement it's just a part of life.

How often do you win? Do you summon the courage to speak your mind? Do you silently give way to this resistenace?

I want you to think back at the end of the day. Find something that you didn't do because you were afraid. The smaller the task, the better. There is another day for the big hurdles.

Tomorrow, or the next time you are in that situation you will know what to do. Once you give it some thought ahead of time, you will find it easier to overcome.

A good example is being afraid to send a text message. The platform does not matter. It's easy and no one is going to mind you posting, except perhaps yourself. Don't dwell, don't take more time - just say what you want to say.

The good thing about challenging yourself, is that you are always around for the exercise. This gives you plenty of opportunity to get better, so long as you want to.

My current story of fear concerns writing. Though I can sit down with a laptop, it is all too easy to distract myself. I am getting better, that is all that matters.

Fear is elusive, it is an abstraction and the reality in which we describe fear, is not always the same.

Fear of running from your life from an angry mob and fear of embarassment. Are these the same fear?

Fear is sometimes felt for a very good reason, i.e being stabbed with a pitchfork. It's important to recognise that not all fear is useless. Sometimes it's good to be afraid.

It's when being afraid prevents you doing something, that you should be afraid. I haven't seen this effectively used but coin a similar idea as FOMO: Fear of Missing Out.

If you allow fear to dominate your thoughts, you allow fear to steal away that which is most precious. Yourself.

How's about that for a bit of spookiness this Halloween. This Halloween, fight that fear..
