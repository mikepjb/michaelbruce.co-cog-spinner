---
title: Time
date: 2019-11-21
publishdate: 2019-11-21
---

How much time do you have?

Are you in time?
As you scroll by,
looking for wisdom,
tricks to apply,

Are you aware?
As time is passed,
There's only so much,
You wished to ask,

How does it feel,
at the end of a day,
when the tasks you planned,
have slipped away,

How does time feel?

Nothing is free,
This one is tricky,
Doing nothing in the moment,
Is freedom.

Freedom from anchors,
Freedom to feel,
The space you wanted,
Just to get real.
