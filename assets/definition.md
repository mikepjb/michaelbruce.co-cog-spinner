Design & Technology

Define yourself

- Michael Bruce
- Building systems that provide value.
- Websites are visual systems that interact with users.
- Find a way to work with people problems and office politics.

Define your key audience

- Myself; building services - an ebook builder
- CEOs/Founders without technical backup.
- Marketers without design backup.

3 Design Goals of the site:

- Convert - send me a message
- Convey what I do - building systems that create you value.
- Showcase systems that do just that (what?)
  - watermarker
  - website (panther)
  - design palette - this is what I use when creating

Imagery

- Candle in the dark
- Sky lanterns
- The wheel.
- Inventions that changed the world.
- Rule of thumb: things that make a big difference.

Copy

Small systems change the world.

Sites, Camera, Action

Second section

name? Showcase?

Secure Content Distribution (example no. 1)

A tiny (200 line) server is all you need to send documents externally in a secure way.

Seal of Approval

## Technical

### SSL/Site Setup

AWS S3 hosting

CloudFront:
  (before starting generate Origin Certificate)
  > uses CloudFlare generated 10 year cert
    - under "Crypto" section of your website, generate Origin Certificate
    - this requires you to use the "Cloudflare Origin CA — RSA Root" as your
      certificate chain (the authority that signed the cert) which can be found
      on the cloudflare support (see instructions for installing on a server
      under Step 4 - Add Cloudflare Origin CA root certificates)
    - Import Cloudflare certificate into ACM
  - set to redirect HTTP to HTTPS
  - Compress Objects Automatically
  - www.michaelbruce.co and michaelbruce.co as alternate CNAMEs
