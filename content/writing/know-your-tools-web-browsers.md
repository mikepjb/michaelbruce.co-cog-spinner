---
title: "Know your tools: Web Browsers"
date: 2018-10-22T00:00:00+01:00
---

# Know your tools: Web Browsers

<div><img src="/images/browser.jpg"></div>

<span class="w-drop-cap">W</span><span>eb Browsers are perhaps the first example you think of when asked which
applications you work with on a computer.</span>

There are plenty about but fortunately for us many of the useful keybindings
work in the same way. You spend thousands of hours using these utilities, by
learning how to control them you are saving yourself time and increasing what
you get out of it.

Below are my tips on using a browser, ideally Firefox or Chrome.

## Let's dive in

<div class="keys-block">
<span class="key shadow">Ctrl / &#8984;</span> <span class="large">+</span> <span
class="key shadow">N</span> <span class="large">and</span> <span class="key
shadow">Ctrl / &#8984;</span> <span class="large">+</span> <span class="key
shadow">T</span>
</div>

These two shortcuts will open a **new window** or a **new tab** respectively.
Most people know this but it's worth covering the basics for those that do not.

<div class="keys-block"> <span class="key shadow">Ctrl / &#8984;</span> <span
class="large">+</span> <span class="key shadow">L</span></div>

This is the bread and butter of optimal browser use, it will **focus the address
bar**, allowing you to quickly search & navigate to other websites from the
keyboard.

No more moving the mouse across the screen every time you want to visit a new
site.

<div class="keys-block"> <span class="key shadow">Ctrl / &#8984;</span> <span
class="large">+</span> <span class="key shadow">A</span></div>

Using this will **highlight all the text** in the address bar.

## Tab Navigation

<div class="keys-block"> <span class="key shadow">Ctrl</span> <span
class="large">+</span> <span class="key shadow">Tab</span> <span
class="large">and</span> <span class="key shadow">Ctrl</span> <span
class="large">+</span> <span class="key shadow">Shift</span> <span
class="large">+</span> <span class="key shadow">Tab</span></div>

These move your **forward and backward one tab**.

<div class="keys-block"> <span class="key shadow">Alt</span> <span
class="large">+</span> <span class="key shadow">1 - 9</span></div>

These move you to **the tab in position x**.

## C-c-c-combo breaker!

These shortcuts compose well into some sharp usability hacks.

Say you want to **visit Twitter in a new tab**:

<div class="keys-block"> <span class="key shadow">Ctrl / &#8984;</span> <span
class="large">+</span> <span class="key shadow">T</span> <span class="large">,</span> <span class="key shadow">Ctrl / &#8984;</span> <span class="large">+</span> <span class="key shadow">L</span> <span class="large">,</span> <span class="key shadow">T</span><span class="large">,</span> <span class="key shadow">W</span><span class="large">,</span> <span class="key shadow">Return</span></div>

Done properly this takes a second to do, when you're done hit

<div class="keys-block"> <span class="key shadow">Ctrl / &#8984;</span> <span
class="large">+</span> <span class="key shadow">W</span></div>

To **close the window**.

## In Summary

Navigating via the keyboard is much faster than using a mouse and will allow you
to access the internet at a pace much closer to your speed of thought. I
recommend you give these tips a try!

If there is enough appetite for this, I will make a part two using plugins like
Vimium that include further improvements.

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/2018-10-22-know-your-tools-web-browsers" />
<meta property="og:title" content="Know your tools: Web Browsers" />
<meta property="og:image" content="https://michaelbruce.co/images/browser.jpg" />
