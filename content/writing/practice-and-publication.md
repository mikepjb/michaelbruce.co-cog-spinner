---
title: Practice and publication
date: 2019-10-28
publishdate: 2019-10-28
---

I'm enjoying writing in the morning, it's good to start the day with something
that feels easier as the week runs on. It'll never be easy.

If it feels easy, you're not trying.

Writing is more enjoyable this morning than I remember it being. Like a breath
of fresh air, it's the activity that starts the background to my day.

Today I have publication on my mind, how often should you publish what you
produce? I'd like there to be a regularity to it, and it shouldn't all be in one
go. I don't know that articles released together are read less often, it is just
a hunch of mine.

No one seems to publish 'in bulk' so to speak. Some publish everyday on one day
and fewer on others.

Is there something about the content I write that suggests a good cadence? Do I
just need to make my mind up.

You just need to make a decision. You can change that decision, but the
important thing is that you move on. Acting on a decision is often more
important that making it.

At least on decisions like this, that are not time sensitive. I could make the
decision at any time in my life. Compare this to deciding to go to an event this
weekend, the value of the decision expires and it can't be changed once the
event happens.

There are decisions you can change.
