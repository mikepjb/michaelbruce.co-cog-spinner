---
title: "Have Confidence to Think"
date: 2018-11-02T00:00:00+01:00
---
# Have Confidence to Think

<div><img src="/images/thinking-girl.jpg"></div>

<span class="drop-cap">T</span><span>hinking is taboo, in a world where AI is always looming around the corner and
tools are provided to answer our every question.</span>

#### *Is it any wonder that people might shy away from using their own mind?*

When I first began developing software, like many in this position I wanted to
improve my skills and become better at what I do.

For a while I struggled, watching around me as there were colleagues with many
years of experience, pushing changes to codebases at great speed and stability.

I had every resource available, Stackoverflow (a popular Q&A site for
development), a purpose built Integrated Development Environment or IDE equipped
with the latest and greatest debugging tools.

But *I wasn't going anywhere fast*.

Then one day, struggling to even navigate a codebase properly it dawned upon me
that I was acting. I wasn't on stage but I was merely doing what I thought I had
to do.

If you do not understand the system, you throw in a breakpoint (a stopping point
in your program) and you execute and review the programs state. Failing that you
take to Stackoverflow and search for the first part of the error gets thrown,
skim the question and if it vaguely looks appropriate then stick the answer code
in and try it out.

Thankfully this didn't continue forever and I have been able to hone my
approach since those dark & frustrating days.

The turning point came when I happened across some advice that Rob Pike received
which he named:

## The Best Programming Advice I Ever Got

Not long after joining Bell Labs, Rob was pair programming with Ken Thompson
(Unix co-inventor) on an graphics application together.

Rob was driving the sessions, using the keyboard and Ken stood behind him as
they programmed. They were working fast, and things broke, often visibly as
would be expected from a graphics program.

When something went wrong, Rob would, as I had done previously, reflexively
proceed to dive into the code, reviewing stack traces, inserting print
statements, using a debugger and breakpoints etcetera.

During this time Ken, would stay quiet. He'd just stand there and do **nothing
but think**, ignoring Rob and the code infront of him.

Each time this happened, Rob noticed that Ken would often understand the problem
before he did, without even having access to the computer or reviewing the code.

The lesson taught here is that **thinking before debugging is extremely
important**.

Ken was building a mental model of the code and when something broke, it was an
error with the model. By thinking about how the problem could happen it was
easier to locate the cause of the error and an effective high level strategy to
address it.

## Satoru Iwata's Instant Patch

Before joining Nintendo and progressing to become it's president, Iwata-sama,
started out as a programmer for the then fledgling company, HAL Laboratories.

It was there that he was involved building many of the games the company had
released. One of these was Balloon Fight, where during a testplay in the
development cycle, Iwata-sama was able to fix the code on the spot and surprised
everyone. The expected time to fix a bug was in the order of hours, sifting
through print statements and working through the code. **Iwata-sama had memorized
the program** and was able to pin point what had gone wrong and fix it
immediately.

## Opinion is divided

Clearly, thinking and memorisation are challenging in situations like these.

Certainly these feats wouldn't be possible if both Ken & Iwata-sama were not
living and breathing their discipline.

In the same world, there are people who will insist on going through the code,
line by line, at an excruciatingly slow pace. They still deliver results, it's
not necessarily a bad approach but it is worth recognising the difference.

*Going through line by line, is a low commitment strategy* in resolving a bug.
It does not require the user to understand much of the system at all, they are
simply waiting, line by line, until something pops up. They will deal with each
issue they come across, with little knowledge of the bigger picture.

*Modelling your system, is a high commitment strategy* in resolving a bug. It
requires the user to *completely* understand the system and they will spend time
deliberating on how the program's execution could have occured within the
confines on the mental model that they have built alongside it.

These people are not superhumans, the techniques they employ however are
superpowers and require focus & energy to pull off.

## Tools affect thinking

I would be remiss if this article ended before talking about this.

Programmer's have effectively created systems and applications since the dawn of
computing without flashy assistant tools.

The systems have not increased in complexity & arguably much of this is hidden.
If you're interested on that front I recommend you look up some talks on old
games development [like this one](https://www.youtube.com/watch?v=kXbMCKMJXXQ)
then compare it to Game IDEs like Unity today.

Rob Pike doesn't use syntax highlighting, a tool many would consider a basic
necessity.

In Lisp *everybody* uses paredit but Paul Graham managed just fine and built
Hacker News, a popular development news site. It is argued that this tool is bad
because it forces poorly written code, [see here](http://ergoemacs.org/emacs/why_i_despise_paredit.html).

What this shows is that **great systems can be built without a complex
environment**. I think such a place is more distracting that it is helpful.

I'll draw the line at spellcheckers.

## In Summary

Now I'm not avocating anyone omit actual tests and safety checks because they
did it in their heads already *but* **thinking is an important asset for any
serious problem solving** whether it is programming or otherwise.

More generally, it is important to always **think before you investigate**. You
might avoid the latter half entirely this way.

I'll close with a related, favourite quote of mine from the late and great Christopher Hitchens on the subject:

<blockquote>
<p>Take the risk of thinking for yourself, much more happiness, truth, beauty, and wisdom will come to you that way</p>
<cite>Christoper Hitchens</cite>
</blockquote>

Until next time!

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/2018-11-02-have-confidence-to-think" />
<meta property="og:title" content="Have Confidence to Think" />
<meta property="og:image" content="https://michaelbruce.co/images/thinking-girl.jpg" />
