---
title: "Effective Notation"
date: 2018-11-01T00:00:00+01:00
---
# Effective Notation

<div><img src="/images/effective-notation.jpg"></div>

<span class="i-drop-cap">I</span><span>n software engineering we have some pretty kick ass tools for writing at our
disposal.</span>

This comes as no surprise seeing as we make a living building tools, so it
follows that some us will build them for our own purposes. They contain some
powerful features that are useful for keeping large amounts of information
organised. It is often the case that we are working with projects that span tens
of thousands of lines of related content, without effective tools to
strategically position this information we would be lost all the time.

This happens in some software projects but it doesn't have to happen to you! By
applying some of the lessons learned in the software world to the notes you
take, I promise you will have an easier time taking notes.

## Naming is important

If a section is not tagged with an appropriate name, then finding the
information when you need it is going to be tough. Names are important because
they allow us to identify larger sections of text and assign a context to them.
For example by labelling this article "Effective Notation", it should be obvious
that the purpose here is to improve our note taking abilities. Taking time to
deliberate on which label to apply to your notes is first on the list as it is
just that useful and a common oversight.

Today is a bad title, dates are only slightly better but quickly fall into
obscurity. A major event or theme works better in the long term.

## Folding & Hierarchy

![folding in action](/images/design-folding.gif)

Grouping code and hiding it under smaller markers is referred to as **folding**,
most programming editors support this feature. I use Vim as you can see in the
example above. It's a great way of consolidating large amounts of information.

Ofcourse hierachy is used by most people to group together information, it's in
a simliar category to naming in that it is a basic that will take you a long way
when employed properly.

## Highlighting

Can be done with colour or font changing to make individual parts **stand out**,
I like to stay subtle generally but this is a great way of creating effective
notes. I especially like the use of **colour in diagramming**, as seen in my
previous post, [What does my system look
like?](/2018-10-27-what-does-my-system-look-like)

## Ergonomic & editable keybindings

In my editor I use:

<div class="keys-block"> <span class="key shadow">Ctrl</span> <span
class="large">+</span> <span class="key shadow">W</span></div>
This will **delete the last word** I typed, how many times do you misspell a
word in a day? This comes in so handy.

<div class="keys-block"> <span class="key shadow">Ctrl</span> <span
class="large">+</span> <span class="key shadow">J</span></div>
This acts like the enter key & means I don't have to move my hands, it's a small
thing but when you're typing all day it adds up. Generally everything I do for
writing text can be driven from the keyboard.

In a special mode you can use a shorthand language with commands like `dip` to
`delete inner paragraph` under your cursor.

This is just a taster for what's possible, it's a deep rabbit hole only limited
by the user.

## Grepping & Regex

There is a *somewhat* standard language for searching through large chunks of
text known as **regex** or Regular Expression and it looks something like this:

`paint.*door`

This will find any grouping of text that has the word paint at the beginning and
door at the end with an unlimited amount of characters in between. That is what
the `.*` means, `.` is a wildcard for any character and `*` indicates that the
regex should find matches for any number of wildcard characters found between
the words `paint` and `door`

Regex is very flexible. You aren't required to remember to exact phrase you want to find.

You can't use it in Word but you can with Google Docs as an optional choice in
search and replace. It's the kind of tool that you become aware of but is very
complex to use and it grows on you over time. This is the last tip because it is
the most complex and not easily available on most general text editing
software.

## Next Level?

Get a programming text editor, most of them are free and are not nearly as scary
as they may first seem. I'd recommend [Microsoft's VSCode](https://code.visualstudio.com/) for the eager and [Emacs](https://www.gnu.org/software/emacs/) or [Vim](https://www.vim.org/) for the brave.

Until next time!

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/2018-11-01-effective-notation" />
<meta property="og:title" content="Effective Notation" />
<meta property="og:image" content="https://michaelbruce.co/images/effective-notation.jpg" />
