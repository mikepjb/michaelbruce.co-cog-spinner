---
title: "Patterns & Waves"
date: 2018-11-03T00:00:00+01:00
---
# Patterns & Waves

<div><img src="/images/patterns-and-waves.jpg"></div>

<span class="drop-cap">I</span> <span>was clearing through my house today, it *always* ends up a mess.</span>

<br>

Overtime, I get on with other tasks, life throws these important & urgent
undertakings our way and you deal with it. I can't keep focussing on tidying my
place but it's a wonderful thing to be in a clean home.

## What can be done?

Hiring someone to me has always seemed like the wrong route to go because of the
cost and fact that you do not know the location of your own possessions when you
want to use them.

I've always been the kind of guy to do things for myself at least once in my
life; **I built my own kitchen** and I'll be damned if I can't find a way to
keep it in good order.

I realise by this point that you may not care for domestic chores & detail. This
is usually the case for me too but I do enjoy a good challenge.

It had occurred to me that my place of residence, a small apartment with five
rooms, managed to get into a mess as I would start a task that required an
object, I would first have to rummage around the room looking for the damn thing
vaguely where I last remembered hiding it.

This sometimes fun but time consuming game would eventually lead me into
starting the originally planned with the required object at hand. Excellent..

After having less time than I would have liked I might put it back where I found
it or keep it close by if I envisaged using it again shortly. The point is that
there is no defined place for it and the cycle of my life continues.

<div class="mark">There are two patterns here, they feed into each other creating waves.</div>

Firstly, the initial root cause of the domestic nightmare is down to having no
defined position for things to be located.

If you imagine writing a program that finds and returns objects for the user,
it's job will be an order of magnitude more expensive if the starting position
and delivery position are not known. If the delivery position is set at random
then previous objects will further complicate the process. This is essentially
how I run my house.

A lookup table would make the searching and returning strategy for objects much
more efficient.

The second pattern, the reason the first problem was possible is due to my own
inability to spot this pattern over a long period of time. This is the more
important self discovery because it is a lesson that has many further
applications.

Sometimes we go through life performing difficult tasks and believe that it's
the best thing to do, perhaps because we think it must be worth the input.

Well it's very likely the task at hand is worth it or you would not keep doing
it.

That does not mean there isn't a better way. These difficult tasks are a
strategic faux pas that as people, we do not easily identify.

I have a good answer for the first pattern but not the second, at least not
today. On reflection, perhaps the act of writing this up will make an impression
on me. I suppose time will tell.

I'll just have to wait for another wave.

## In Summary

If you want to get more done and tidy less then you might consider labelling
boxes & drawers so you can give your household items a place to live. I am not
good at intuitively spotting these kinds of patterns over a long period of time.
I am not sure how to improve my hitrate for detecting such things.

## Thank you

Thanks for reading, I take a lot of enjoyment pulling apart life's complex
problems, particularly when it comes to technology. If you liked this article,
perhaps you'd like my help building your next site or system. You can get
started by using the form below.

Until next time!

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/2018-11-03-patterns-and-waves" />
<meta property="og:title" content="Patterns & Waves" />
<meta property="og:image"
content="https://michaelbruce.co/images/patterns-and-waves.jpg" />
