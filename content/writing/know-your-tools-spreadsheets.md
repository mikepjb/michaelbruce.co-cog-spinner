---
title: "Know your tools: Spreadsheets"
date: 2018-11-24T00:00:00+01:00
---
# Know your tools: Spreadsheets

<div><img src="/images/know-your-tools-spreadsheets.jpg"></div>

<span class="f-drop-cap">F</span><span>or a while, I tried to commit to only using
plain text files for all my documents, spreadsheets are one of the few
exceptions to this and it's easy to see why. Spreadsheets, particularly Excel
are a staple program for every computer user.</span>

Today I'll be sharing my tips on **how to use Google Sheets**:

### Tip #1: Edge Jumping

<div class="keys-block">
<span class="key shadow">Ctrl / &#8984;</span> <span class="large">+</span> <span
class="key shadow">&larr;</span> <span class="large">/</span>
<span class="key shadow">&uarr;</span> <span class="large">/</span> 
<span class="key shadow">&darr;</span> <span class="large">/</span> 
<span class="key shadow">&rarr;</span>
</div>

This will move you **to the edge** of a block of cells like so:

<img src="/images/tip-1-edge-jumping.gif">

This is a bread and butter move that you'll find yourself using a lot, just this
one tip alone will help you out significantly.

### Tip #2: Edge Selecting

<div class="keys-block">
<span class="key shadow">Ctrl / &#8984;</span> <span class="large">+</span>
<span class="key shadow">Shift</span> <span class="large">+</span>
<span class="key shadow">&larr;</span> <span class="large">/</span>
<span class="key shadow">&uarr;</span> <span class="large">/</span> 
<span class="key shadow">&darr;</span> <span class="large">/</span> 
<span class="key shadow">&rarr;</span>
</div>

Applying shift to the above command **highlights as you move**.
Combining the above allows you to select entire blocks with ease.


<img src="/images/tip-2-edge-selecting.gif">

### Tip #3: Insert/Delete Rows & Columns

<div class="keys-block">
<span class="key shadow">Ctrl / &#8984;</span> <span class="large">+</span>
<span class="key shadow">Alt / Option</span> <span class="large">+</span>
<span class="key shadow">=</span> <span class="large">/</span>
<span class="key shadow">-</span> <span class="large">&rarr;</span> 
<span class="key shadow">r</span> <span class="large">/</span> 
<span class="key shadow">c</span>
</div>

<img src="/images/tip-3-insert-delete-rows.gif">

### Tip #4: Formatting Cells

Fairly obvious but worth including:

<div class="keys-block">
<span class="key shadow">Ctrl / &#8984;</span> <span class="large">+</span>
<span class="key shadow">b</span> <span class="large">/</span>
<span class="key shadow">i</span> <span class="large">/</span> 
<span class="key shadow">u</span>
</div>

**Embolden, italicise & underline** the highlighted cells respectively.

Lesser known are:

<div class="keys-block">
<span class="key shadow">Ctrl / &#8984;</span> <span class="large">+</span>
<span class="key shadow">Shift</span> <span class="large">+</span>
<span class="key shadow">e</span> <span class="large">/</span>
<span class="key shadow">l</span> <span class="large">/</span> 
<span class="key shadow">r</span>
</div>

These will **change the alignment**, centering, left align or right align respectively.

Finally:

<div class="keys-block">
<span class="key shadow">Shift</span> <span class="large">+</span>
<span class="key shadow">Alt</span> <span class="large">+</span>
<span class="key shadow">1</span> <span class="large">/</span>
<span class="key shadow">2</span> <span class="large">/</span> 
<span class="key shadow">3</span> <span class="large">/</span> 
<span class="key shadow">4</span>
</div>

**Add a border** to the top, left, bottom, right edges respectively. Putting
this all together we can do something like this:

<img src="/images/tip-4-formatting-cells.gif">

Unfortunately I do not know of a way to colour format with a shortcut.

### Tip #4: Quick Date

<div class="keys-block">
<span class="key shadow">Ctrl / &#8984;</span> <span class="large">+</span>
<span class="key shadow">;</span>
</div>

**Prints the date**, simple.

### Tip #5: Toggle Formula Display


<div class="keys-block">
<span class="key shadow">Ctrl / &#8984;</span> <span class="large">+</span>
<span class="key shadow">`</span>
</div>

Very useful for **building your mental model** of how a spreadsheet works.

<img src="/images/tip-6-toggle-formula-display.gif">

### Tip #7: Switch numerical formats

<div class="keys-block">
<span class="key shadow">Ctrl / &#8984;</span> <span class="large">+</span>
<span class="key shadow">Shift</span> <span class="large">+</span>
<span class="key shadow">1</span> <span class="large">/</span>
<span class="key shadow">2</span> <span class="large">/</span> 
<span class="key shadow">3</span> <span class="large">/</span> 
<span class="key shadow">4</span> <span class="large">/</span> 
<span class="key shadow">5</span>
</div>

Switches a numerical entry between **regular, time, date, currency & percentage
formats**, respectively.

<img src="/images/tip-7-switch-numerical-formats.gif">

### Tip #8: Strikethrough

Last but not least, the <b><del>strikethrough</del></b>. Very nice for ticking off items, and keeping your progress in perspective. The shortcut for this is 

<div class="keys-block">
<span class="key shadow">Alt</span> <span class="large">+</span>
<span class="key shadow">Shift</span> <span class="large">+</span>
<span class="key shadow">5</span>
</div>

<img src="/images/tip-8-strikethrough.gif">

### In Summary

Like all shortcut learning endeavours, it's useful to **write a few important
shortcuts** on a postit and keep it under your monitor to remind you and commit
to it becoming a habit. Then you'll the see the rewards when this becomes second
nature.

Do you have any essential tips I missed? Let me know [@mikepjb](https://twitter.com/mikepjb) on twitter or in the contact form below.

### Thanks for reading

I hope you'll find these shortcuts as handy as I do.

If you're looking for a **shortcut improving customer engagement** & sign ups on
your website, I'd love to help you make it better.
If you're interested, **get in touch**.

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/2018-11-24-know-your-tools-spreadsheets" />
<meta property="og:title" content="Know your tools: Spreadsheets" />
<meta property="og:image" content="https://michaelbruce.co/images/know-your-tools-spreadsheets.jpg" />
