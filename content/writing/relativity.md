---
title: Relativity
date: 2019-11-01
publishdate: 2019-11-01
---

You are small in the universe, even when you make it big. There's lot to do and there's lots of see.

I am reminded of this most in London, where you catch trains that take you huge distances. If you don't get on in time, it will leave without you. Everyone else moves on.

Life is like this, you may be small but that doesn't mean you can't win big. You will already have made a tiny dent in the universe as you read this. Think of the people you know, how life is different just by you being there.

You are part of the universe, each of us a tiny groupings of atoms. Much of us isn't even human, the bacteria in our gut outnumbers us.

Yet the living cells in your gut keep something much bigger than themselves alive. In return for a home, they help us break down and get nutrients from food.

Bad gut health affects the health of the human host. Just like bacteria, you can affects systems bigger than yourself.

Beekeepers engineer thousands of Bee colonies to create honey. Not quite enough to meet US demand, but we are talking about a scale that contributes to feeding the entire world.

In many cases we see that, huge proportions of foods are grown from single locations. Rose water from Bulgaria, Chocolate from ..french africa? The list goes on.

When you're thinking about your ability to make change, I want you to believe that big changes are not only realistic, that they happen all the time.

From the time you were born to the momeent you were reading this, you've come a long way.

There are plenty of people out there, ready to accept defeat. That life has no meaning, seeing small as next to nothing. It is not, small is still big enough to be distinct. A crowd is made from many people.

I can understand it. It takes each person in a group to stand together and make a crowd. It is natural to group things together and forget the parts that make it whole.

It is why statistics can mislead, i.e Women getting paid less on average might suggest we are an unfair bunch. It can be enough for people to resent others, since they feel the issue is out of their control.

Assertiveness however, affects your pay more than what is between your legs. Men on average are more assertive, there is no reason why an assertive woman cannot compete, and even succeed over her male competition.

It's important to keep a healthy perspective on life. You are small in the world, but you should be thinking big. Being in a big world, means that there are an abundance of opportunities.
