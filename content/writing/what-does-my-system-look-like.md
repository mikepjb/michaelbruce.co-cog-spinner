---
title: "What does my system look like?"
date: 2018-10-27T00:00:00+01:00
---
# What does my system look like?

<div><img src="/images/system-diagram.jpg"></div>

<span class="l-drop-cap">L</span><span>ast week I started designing a content delivery system, it will be a site than
can be used to distribute media across multiple platforms in a time sensitive &
fault tolerant manner.</span>

If you've built a system before, you'll know that having an idea of what your
service will look like before you start building is essential. We're visual
creatures and visualisation goes a long way in helping us understand what we are
doing. The better you can picture the system, the better you will be able to
make high level, strategic decisions on how it should be effectively designed.

From this perspective, creating maps to model your system seems necessary, at
the very least it's a great way to set yourself up for success. Why would you
not want this?

Below are key points you might consider when creating your own, further down I've used my content delivery system map as an example to illustrate at the end
of this article.

## Your system is abstract

The first hurdle here is that systems are not physical, in the sense that they
have no defined shape. Unlike electrical diagrams & blueprints with their
physical counterparts, it's up to us to define the visual representation.

There is very little common language in system mapping, so I'd recommend you
choose your own with one thing in mind; Aim to make your mappings intuitively
readable, which is a challenge but is still a useful exercise.

## Models should map to it's created form

Whether this is code in software or another medium, if your model does not have
a definite link to the building blocks you will be using, then you will have no
foundation to build upon.

Grounding your system to ensure that the ideas represented in your system can be
brought to life might seem obvious but is often forgotten. For example if your
system will be gathering information from an external source you know that an
API is required.

We want to avoid magic maps where stuff "just happens" without explanation.

## Map at the appropriate level

If you are navigating toward a location in a nearby town and you find yourself
at a fork in the road, you get a map out to assist.

You might be able to find your way from a map of the town, but a county/state
size wouldn't work. Ideally you'd like a map local to the square mile that
you're currently in.

Resolution makes a huge difference in a maps utility.

## Example Map
### Content Delivery System

<div class="map-container">

<div class="map-level">
<div class="map-user map-element">
Content Creator
<div class="map-desc">The user of the system.</div>
<div>
<div class="map-path-red">&nbsp;</div>
<div class="map-path-green">&nbsp;</div>
<div class="map-path-blue">&nbsp;</div>
</div>
</div>
</div>

<div class="map-level">
<div class="map-element">
User uploads and prepares content
<div class="map-desc">Using a built-in text editor, the user is able to create
and save content for later distribution. The user is also able to see what
the post preview will look like using the open graph protocol.</div>
<div><div class="map-path-red">&nbsp;</div></div>
</div>
<div class="map-element">
User configures the distribution of prepared content.
<div class="map-desc">Configures which distributors to use & at what time.</div>
<div><div class="map-path-green">&nbsp;</div></div>
</div>
<div class="map-element">
User views stats from distributed content in dashboard.
<div class="map-desc">Review the response to the content.</div>
<div><div class="map-path-blue">&nbsp;</div></div>
</div>
</div>

<div class="map-level">
<div class="map-element mono">
POST /upload
<div class="map-desc">Using standard net/http lib with parse multi form to
handle upload in a standard form.</div>
<div><div class="map-path-red">&nbsp;</div></div>
</div>

<div class="map-element mono">
GET /content
<div class="map-desc">Retrieve information about a creator's content.</div>
<div>
<div class="map-path-red">&nbsp;</div>
<div class="map-path-green">&nbsp;</div>
</div>
</div>

<div class="map-element mono">
POST /distribute
<div class="map-desc">This requests that the service should start distributing
the user's content with the supplied options.</div>
<div><div class="map-path-green">&nbsp;</div></div>
</div>

<div class="map-element mono">
GET /history
<div class="map-desc">Collects a series of distributions sent out by the user</div>
<div><div class="map-path-blue">&nbsp;</div></div>
</div>

<div class="map-element mono">
GET /stats
<div class="map-desc">Collects a series of distributions sent out by the user</div>
<div><div class="map-path-blue">&nbsp;</div></div>
</div>

<div class="map-element mono">
Background, collect stats
<div class="map-desc">Cache feedback of distributed content from endpoints to ensure a responsive & consistent service.</div>
<div><div class="map-path-purple">&nbsp;</div></div>
</div>
</div>

<div class="map-level">
<div class="map-element map-db">
Postgres
<div class="map-desc">
Holds uploaded & written content for each user.
</div>
<div>
<div class="map-path-red">&nbsp;</div>
<div class="map-path-green">&nbsp;</div>
</div>
</div>

<div class="map-element map-db">
Social Media
<div class="map-desc">Twitter, Facebook, Instagram & Linkedin each have developer APIs. Can post links with og preview cards and collect stats.</div>
<div>
<div class="map-path-green">&nbsp;</div>
<div class="map-path-blue">&nbsp;</div>
<div class="map-path-purple">&nbsp;</div>
</div>
</div>

<div class="map-element map-db">
Email
<div class="map-desc">Using email marketing services such as Sendgrid &
Sparkpost. Sends emails & returns statistics.</div>
<div>
<div class="map-path-green">&nbsp;</div>
<div class="map-path-blue">&nbsp;</div>
<div class="map-path-purple">&nbsp;</div>
</div>
</div>

<div class="map-element map-db">
Site integration
<div class="map-desc">Post to typical wordpress, jekyll & hugo sites</div>
<div>
<div class="map-path-green">&nbsp;</div>
<div class="map-path-blue">&nbsp;</div>
<div class="map-path-purple">&nbsp;</div>
</div>
</div>
</div>

<div class="legend">
Legend:
<div><span class="map-user sample">&nbsp;</span> User (Layer 0)</div>
<div><span class="map-view sample">&nbsp;</span> View (Layer 1)</div>
<div><span class="mono sample">&nbsp;</span> Action (Layer 2)</div>
<div><span class="map-db sample">&nbsp;</span> Storage/External (Layer 3)</div>

<div><span class="map-path-red">&nbsp;</span>Red Storyline</div>
<div><span class="map-path-green">&nbsp;</span>Green Storyline</div>
<div><span class="map-path-blue">&nbsp;</span>Blue Storyline</div>
<div><span class="map-path-purple">&nbsp;</span>Purple Storyline</div>
</div>
</div>

This is an **overview** map of my planned system.

It has **storylines**, represented by the coloured dots which all reach back to the
content creating user except for purple which is used for background tasks.

Storylines are linking vertical components together by context and help me to understand
why they should be part of the system. This is helpful in keeping the design
lean, resulting in a more efficient build of software.

**Layers** group components horizontally by role. I consider users to be part of the system but this isn't the matrix so nobody needs saving today. Without users the system cannot operate so it seems natural to include them.

There are 4 layers:

- Layer 0, The User - In this case a content creator, no surprises here.
- Layer 1, Views - A user needs a view of the system, since it is abstract we
  need to build a manifestation for each use case. Practically you might call
  this a web page, e.g the Content History page.
- Layer 2, Actions - A view can do nothing without being able to perform
  actions. These are typically requests that are made to a server.
- Layer 3, Storage/External - Often services must connect to external systems to
  perform actions, there are APIs and interfaces to each of these. For example a
  database is very useful for persisting a users created content, we don't need
  to build a database because several products already exist. Postgres is a fine
  choice here.

This grouping creates a grid of relations between the components, it's clearer
now that each part is able to cohesively bond together to form the system.

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/2018-10-27-what-does-my-system-look-like" />
<meta property="og:title" content="What does my system look like?" />
<meta property="og:image" content="https://michaelbruce.co/images/system-diagram.jpg" />
