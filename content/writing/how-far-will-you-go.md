
---
title: How far will you go?
date: 2019-11-07
publishdate: 2019-11-07
---

# How far will you go?

How far will you go? How many times have you started something and never
finished? When is a job worth doing properly?

I've thought for a while that it's a good thing to be able to let go of your
work. That looking the sunk cost in the face and saying it was time to go.

I still think it is, but there is a balance. If you allow 

no completion to become the norm then .. it is by definition a setup for failure

training yourself to fail, training yourself that things don't mean anything

that starting again is valuable.
