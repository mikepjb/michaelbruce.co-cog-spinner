---
title: "Color Psychology"
date: 2018-11-26T00:00:00+01:00
---
# Color Psychology

<div><img src="/images/color.jpg"></div>

<span class="a-drop-cap">A</span><span>n important aspect of design & one that I
often find myself wondering about is color. These vibrant patterns **hold our
attention** & have the power to **influence behaviour**. Many of the articles
online didn't go into detail, or reference their claims so I did some research.</span>

This is a pretty comprehensive article, it's broken down into four parts:

1. <a href="#part1">The Character of Color</a>
2. <a href="#part2">Audience Perception</a>
3. <a href="#part3">Color Profiles</a>
4. <a href="#part4">Composition & design</a>

<div id="part1"><h2>1. The Character of Color</h2></div>

The most important section is this one, only by understanding the attributes
that make a color can you make use of them. For example, it is possible to
have both <span style="background-color: #69f; padding: 2px">cool</span> & <span
style="background-color: #6fc; padding: 2px;">warm</span> blues.

There are three attributes we use to describe color:

  - **Hue**, the dominant wavelength or **base color** if you will. We commonly
    refer to these as colors. Hue is the spectrum of what we are able to see
    with our eyes.
  - **Value**, also known as tone or lightness, is **how bright we perceive a
    color** to be.
  - **Saturation**, a combination of the previous characteristic, **value & the
    distribution of hue** across the spectrum of wavelengths. The more
    exclusive a hue is & a higher value result in high saturation.

### Hue

There are a lot of hues that we can see but we tend to refer to them under regions
of
<span style="background-color: #f00; padding: 2px"><b>red</b></span>,
<span style="background-color: #609; padding: 2px; color: #fff;"><b>violet</b></span>,
<span style="background-color: #00f; padding: 2px; color: #fff;"><b>blue</b></span>,
<span style="background-color: #0f0; padding: 2px;"><b>green</b></span>,
<span style="background-color: #ff3; padding: 2px;"><b>yellow</b></span>,
&
<span style="background-color: #f60; padding: 2px;"><b>orange</b></span>. In reality there are **countless
combinations**. Closely related hues share attributes, described below:

#### Temperature

Short wavelength hues, blues, violets & greens, are known as **cool hues** that
help to relax the observer<a href="#1"><sup>[1]</sup></a>. They are **generally preferred**<a href="#2"><sup>[2]</sup></a> over the more arousing
warm counterparts of red, yellow & orange.

Temperature is a spectrum, you will get warmer greens the closer you get to
yellow and cooler reds the closer you get to violet. It has been shown to **affect
the heartrate**<a href="#3"><sup>[3]</sup></a> of the audience seeing cool & warm colors.

We have brake lights & warning signs colored in red because it aids in bringing
an urgent attention to the user.

<div id="part2"><h2>2. Audience Perception</h2></div>

Perception is subjective & so to truly get the choice of color correct, we must
consider our audience.

### Preference hierarchy

<div style="background-color:#00f; padding: 2px; color: #fff; width: 200px">Blue</div>
<div style="background-color:#0f0; padding: 2px; width: 200px">Green</div>
<div style="background-color:#609; padding: 2px; color: #fff; width: 200px">Violet</div>
<div style="background-color:#f00; padding: 2px; color: #fff; width: 200px">Red</div>
<div style="background-color:#f60; padding: 2px; width: 200px">Orange</div>
<div style="background-color:#ff3; padding: 2px; width: 200px">Yellow</div>

This is the order in which most people prefer color<a
href="#4"><sup>[4]</sup></a>. In the same study, **colors in the black region were
preferred over white**. Some cultures prefer red and/or green to blue but the
ranking otherwise holds true.

No matter the hue, higher brightness & saturation are positively related to how
likeable a color is<a href="#4"><sup>[4]</sup></a>.

**Brightness & saturation** have substantially greater effects on emotions than hue<a
href="#5"><sup>[5]</sup></a>.

### Personality

- Extroverts generally prefer brighter colors, wheras introverts prefer lighter,
  more subdued tones<a href="#6"><sup>[6]</sup></a>.
- Extroverts are very responsive to color, introverts are more sensitive to
  shape. However color is more persuasive than shape is regardless<a href="#7"><sup>[7]</sup></a>.

### Learned response & Nature

- The world's most popular color is blue, followed by green and is abundant in
  nature. The world is filled with blue seas & green vegetation. Like written
  copy, **color resonates better when we can relate to it**<a href="#8"><sup>[8]</sup></a>..
- negative responses to gray, associated with rainy, overcast
  days.
- **Religion also plays a significant role**: Hinduism/Buddhism/Confucianism
  promoted yellow/orange. Islam/Celts green & Christianity red/white & black<a href="#7"><sup>[7]</sup></a>.
- White for weddings
- Red to mark mistakes.
- Figures of speech: green with envy & seeing red, influence color evaluation.

### Context matters

Also known as "color-object appropriateness".

Generally speaking people prefer blue over yellow, except in the context of a
lemon or a wedding band. By tradition we prefer writing in blue, not red pen
ink.

### Age

Lighter colors work better with older audiences as people age, colors begin to
look darker<a href="#7"><sup>[7]</sup></a>.

### Perception has huge variance.

- Green holds vastly different meanings between desert & rainforest dwellers<a href="#7"><sup>[7]</sup></a>..
- White for tropical & polar residents<a href="#7"><sup>[7]</sup></a>.
- What is universal is that people seem to **associate color with their
  environment**.

### Social status

- Blue collar audiences prefer primary colors, more upscale audiences prefer
softer pastels<a href="#9"><sup>[9]</sup></a>.
- Silver, gold & red all convey a sense of quality that appeals to sophisticated
audiences.
- Black is very formal, chic & serious used for reaching white collar demos<a href="#10"><sup>[10]</sup></a>.

<div id="part3"><h2>3. Color Profiles</h2></div>

### Red

Red is both associated with positive & negative emotions, my personal thought
here is that value & saturation play a big part here. A very cool <span
style="background-color: #903; color: #fff; padding: 2px;">purple like
red</span> will look sadder than <span style="background-color: #f30; color:
#fff; padding: 2px;">sunburnt red</span>.

It is a **very powerful color**, tipping the tide of sporting events<a href="#11"><sup>[11]</sup></a>, **promoting
hunger & raising heartrate**<a href="#12"><sup>[12]</sup></a>. It is no surprise
that red & yellow are the most popular choices for fast food branding.

It makes **objects seem heavier** & time pass slowly<a href="#13"><sup>[13]</sup></a>.

It is very intimidating, for this reason wearing red is not suitable if you are
seeking employment but will work in your favour if your goal is to lead. The
concept of **the red power tie is real**.

Red sells because it conveys quality & makes you want to buy<a href="#13"><sup>[13]</sup></a>.

### Yellow

Color theorists agree that yellow is a happy hue - It has connotations of sun,
brightness & warmth. Ironically it is also the least-preferred hue. In <span style="background-color: #ffc; padding: 2px"> soft,
creamy tones</span>, yellow is soothing. It often appears <span style="background-color: #ff0; padding: 2px">brighter than white</span> to the
human eye.

Similarly good for drawing attention with warnings, it also raises the pulse,
but not to the same extent as red<a href="#13"><sup>[13]</sup></a>.

### Orange

A mix between Yellow & Red, the **bright yet powerful** properties have made it a
**popular color for marketing cleaning products**.

### Blue

Blue stands opposed to Red is almost every single way.

- Lowering blood pressure<a href="#14"><sup>[14]</sup></a>.
- Stimulating creativity.
- Stifling hunger.
- Objects seem lighter & time passes more quickly<a href="#13"><sup>[13]</sup></a>.
- Very popular in clothing, accounting for about half of all combinations.
- Blue is popular for traditional restaurants because it relaxes customers and
  encourages them to linger, making them more likely to add to their orders.

### Green

Green is quite nuanced in that it's psychological effects vary widely depending
on the tone used.

- Bright greens are associated with nature, trees, which is often relaxing.
- Dark green connotes wealth & status.
- Pea green (a bright green) is also associated with sickness.
- Also associated with death, poison, supernatural & aliens.

<div id="part4"><h2>4. Composition & Design</h2></div>

Now we are more familiar with color, let's take a closer look at how to combine
them & use them in design.

### Scheme variance

The number of colors you choose to include in a design affects the overall feel:

- **monochromatic designs**, containing various shades/tints of one hue are
  effective for **communicating simple messages**.
- wide variety color palettes can add depth, complexity & additional meaning to
  design<a href="#15"><sup>[15]</sup></a>.

### Color combinations

<img src="/images/wheel.png">

- **Colors opposite each other** on the color wheel compliment each other,
  because they **provide contrast** & make them stand out more, each pair has
  both a warm & cool hue.
- 3 hue harmonies allow for a third color, adding complexity whilst still
maintaining a good amount of contrast. You might choose to weight two colors
closer together to make the third choice stand out above them.
- Vibrating colors - when bright colors of equal intensity are placed beside
each other, text becomes harder to read.

### Color is part of your message

Color too should be used as part of the conversation you'd like to have with
your audience. This does not mean that shouting won't work but there are a
variety of ways of using color.

You can personalise through color & converse with more specific audiences,
Forming an intimate connection between you & your audience.

  - 4 color schemes generally are more attractive than one or two color schemes<a href="#7"><sup>[7]</sup></a>.
  - Single color schemes, however, were ranked more tasteful<a href="#7"><sup>[7]</sup></a>.

### Positioning & Refinement

Color helps to **distinguish your identity** from the competition, this is
extremely useful when directly next to them, products in a supermarket or booths
at an event.

People scan before they read. Color sets the tone before you can even start to
comprehend the copy.

### Logos

Using a preferred color for a logo really helps a person being able to recall
it.

A logo should be able to stand on it's own in grayscale before including
hue for two reasons, first is that emotions are more affected by value &
brightness<a href="#8"><sup>[8]</sup></a>, second is that the shape is also important.

White, black, gray & silver can work well in the logo as constrasts to the
signature color. You should **own** a color. It is a silent salesmen & the
supreme court has ruled that colors alone serve as legally defensible
trademarks.

Competitors in a given market do this to stand out. Consider telecoms EE, Orange, Vodafone & O2 in the UK.

### In Summary

Choosing colors is hugely important in design & should not be taken lightly. The
universal rules of color are united by the audiences relation to the stimulus
rather than it's inherent nature.

After writing this I think that choosing color for your site can only be done by
knowing the type of customer you want to target first.

Color choice depends on 1. Your audience & 2. Your message

After reading on this subject for a little while it became very apparent that
context matters as much as the specific color used.

### Thanks for reading

Wow that was a long one! This started out as a small investigation & snowballed
into the words on this page.

Want help using color to create more engaging content? Let me know with the
contact form below.

Until next time!

### References:

[1]: <a id="1" href="https://www.tandfonline.com/doi/abs/10.1080/00221309.1964.9920584">The Effects of Red and Green Surroundings on Behaviour</a>  
[2]: <a id="2" href="https://www.jstor.org/stable/1417683?seq=1#page_scan_tab_contents">A Critical and Experimental Study of Colour Preference</a>  
[3]: <a id="3" href="https://onlinelibrary.wiley.com/doi/pdf/10.1002/col.21949">The influence of color on student emotion, heart rate, and performance in learning environments</a>  
[4]: <a id="4" href="https://www.jstor.org/stable/1419491?seq=1#page_scan_tab_contents">A System of Color-Preferences</a>  
[5]: <a id="5" href="https://pdfs.semanticscholar.org/4711/624c0f72d8c85ea6813b8ec5e8abeedfb616.pdf">Effect of Color on Emotions</a>  
[6]: <a id="6" href="https://www.emeraldinsight.com/doi/abs/10.1108/03699429910252315">The meanings of colour: preferences among hues</a>  
[7]: <a id="7" href="https://digitalcommons.liberty.edu/cgi/viewcontent.cgi?referer=https://scholar.google.co.uk/&httpsredir=1&article=1118&context=honors">Color Psychology and Graphic Design Applications</a>  
[8]: <a id="8" href="https://psycnet.apa.org/record/1996-01791-004">A note on adults' color–emotion associations</a>  
[9]: <a id="9" href="http://cardinalscholar.bsu.edu/handle/handle/181010">Social class color preferences and application by clothing retailers in segmenting their markets</a>  
[10]: <a id="10" href="https://trove.nla.gov.au/work/5866069?q&versionId=6810135">Global graphics: Color: A guide to design with color for an international market</a>
[11]: <a id="11" href="https://www.tandfonline.com/doi/full/10.1080/02640414.2015.1064156">The colour of a football outfit affects visibility and team success</a>  
[12]: <a id="12" href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4383146/">Color and psychological functioning: a review of theoretical and empirical work</a>  
[13]: <a id="13" href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.476.2928&rep=rep1&type=pdf">Impact of color on marketing</a>  
[14]: <a id="14" href="https://www.sciencedaily.com/releases/2018/11/181108110032.htm">Blue light can reduce blood pressure, study suggests</a>  
[15]: <a id="15" href="https://books.google.co.uk/books/about/Color.html?id=SzJHAQAAIAAJ&redir_esc=y">Color: The Secret Influence</a>  
[16]: <a id="16" href="https://color-wheel-artist.com/complementary-colors-defined/">Complementary Color Schemes – Color Theory and Painting Tips</a>
[17]: <a id="17" href="https://corporate.findlaw.com/intellectual-property/u-s-supreme-court-decides-colors-alone-may-be-registered-as-a.html">U.S. Supreme Court Decides Colors Alone May be Registered as a Trademark</a>

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/2018-11-26-color-psychology" />
<meta property="og:title" content="Color Psychology" />
<meta property="og:image" content="https://michaelbruce.co/images/color.jpg" />
