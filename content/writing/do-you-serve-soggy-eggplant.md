---
title: Soggy eggplant
date: 2019-10-25
publishdate: 2019-10-25
---

# Soggy eggplant

We had lunch recently, at a restuarant situated in a 5 star hotel. I wanted to have a nice salad for lunch. On the top were mozzerella, croutons and crisp peppers. Underneath was something quite different.

About half of the served salad was soggy eggplant, lying underneath the otherwise fresh ingredients. It was cold, it was slimey, it was not good.

So I raised the issue with one of the waiters. He brought over the manager to speak with us.

Half a bowl filled with cold, soggy eggplant was not the salad I was expecting. The manager remedied the situation by suggesting that it was my fault for not ordering the salad hot. Microwaved salad is not my cup of tea, thanks.

Why am I telling you this story? Many of us make products or provide services that others consume. I love to cook and enjoy serving food that tastes good, both for myself and others who are willing to take the risk.

When a chef does not care for food they serve it gets to me. When I create software, the delivery should meet the expected outcome. Nobody wants emails delivered without titles. In just the same way food should taste delicious.

Do you care about the products you produce? Or the service you provide?

The only answer, if you are interested in delivering such things is yes. If care is not given to the product, you will pay the price. Lost reputation is bad for business.

It gets worse when you double down. It is a second mistake to excuse bad delivery and much worse than the first.

People on occasion will make mistakes, often they will make it right for those affected. When they do not, it sends a clear message that you do not care about what you do.

If a restuarant does not care about the food that is served, why eat there? Would you buy from a car dealer that does not check for faults?

Do your best to avoid serving soggy eggplant. If you do, recover from it, by making up for it.
