---
title: "Have your own back: How standing up for yourself is keeping you alive"
date: 2019-10-27
publishdate: 2019-10-27
---

# Have your own back: How standing up for yourself is keeping you alive

<div><img src="/images/have-your-own-back.jpg"></div>

Having left full-time education, I find myself learning more than ever.

Confidence is a powerful thing, it isn't fixed either. You may be more confident at home with your family. Good friends will nurture this feeling in you too.

If you can't be confident somewhere, you shouldn't stay there. You either change the place itself or leave. If you can't be confident, you can't be yourself.

*Why do I think this way?*

An insecure person, does not behave as he or she wants to. They instead, imitate someone who fits into the group. When you aren't you, in a way you are not living. It's a recipe for disaster.

So if you aren't happy in your environment, how do you change it? Here are a few tricks I've used in the past:

### Stand up for yourself

It's not a manner of speaking, standing changes the dynamic in social situations. This is especially important when seated at a desk, and someone is standing behind you. By levelling the playing field and standing up, you make it easier to make your own opinion heard.

### It's never about you

Everyone is self-interested, even the selfless. Bear this in mind when people communicate with you. They care more about what they get and less about how it affects you. They might not even realise how difficult something is for you.

This isn't the same as not caring at all, it's a matter of priority. If you let someone know why you can't do something, they'll usually listen. The key is to avoid being personal. Name calling and assuming bad intentions are both good ways to shoot yourself in the foot.

It's all the same, because everyone is self-interested they do not like hearing that you think they are bad actors. Even if you have reason to believe they are intentionally bad, giving them the opportunity to appear good is too tempting for most.

It's good that people are self-interested.

### Prepare to leave

You need a no deal on the table. For work, having some money tucked away allows you to leave and still pay the bills. Even if you aren't planning to leave, having that option changes how you interact with the world. You no longer feel forced to do things and that makes all the difference.

Unless you are working at C-level or as a head of department, anything longer than a months notice to leave is not a good sign. You can (and should) negotiate something shorter. If a prospective employer refuses to do so, they have trouble retaining staff and long notice periods are not the solution.

### Don't accept the status quo

Just because it is normal, doesn't mean you can't change it. We've always done
it this way is not an argument. It is however something I hear repeated too
often.

Life is too short to live someone else's life. That is what you choose to do when living in a negative environment.

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/writing/have-your-own-back/" />
<meta property="og:title" content="Have your own back: How standing up for yourself is keeping you alive" />
<meta property="og:image" content="https://michaelbruce.co/images/have-your-own-back.jpg" />
