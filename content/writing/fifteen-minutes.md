---
title: Fifteen minutes
date: 2019-10-29
publishdate: 2019-10-29
---

# Fifteen minutes

*This is not meant to be read, it is the result of an exercise I did to improve the flow of my writing*

What do I want to write about?

There are a lot of things I could say. There are a lot of things I want to say.

I want to talk about my cat, who has just jumped into a box. Her name is Mia and she loves playing games. She also likes to curl up in a ball, preferably in a hard to reach place.

The glasses I am wearing, cost me 25 pound. I didn't know how different the world appeared until I was 28. I have astigmatism, where my eyes are not optimal spheres. They work just fine though.

Sometimes I forget I am not wearing glasses in the morning. I sit down at my desk and then realise an hour later, things look a little fuzzy.

How long does it take to write 500 words? That stick to a single topic? Without doubting what you are writing, I think we could be done in 10 minutes.

Today the commons are discussing a December election, that should be fun. Cross party talks feel like a game of tag these days, one side will tag the other without anything changing.

It's very quiet today, even now I am filtering out a lot of what I am thinking. The level of thinking that doesn't make it to the screen is too damn high. I am going to leave this unedited as an example.

What is the purpose of this unedited writing example? The primary reason is to hit another 500 word article but this time as fast as possible.

I am training myself to write faster, to doubt myself less often. This will make me a better writer. Not because I write more, but because I am actually writing.

The difference being that before, when I am paused and looking out the window I am distracted. When I am talking or looking something up online I am distracted.

By writing without pausing, I plan to get content out that during the editing phase I might crystalise into something that is better than I have written previously.

It is better to write something or type something out and it become wrong, misspelled or otherwise. Keeping flow is something I could do much better at.

This is what I am focusing on now, not rewriting, just writing. Let the words flow.

Flowing words from these fingers,
Let them take form on this page,
Is this a wizards spellbook,
Or just babble beyond the grave.

Am I even writing with concious,
Am I even looking,
What are these words before me,
Who wrote them?

I like poetry, it's good fun. It's more than good fun, it's something you can do with just your mind. It's very easy to do, just make it up as you go along and BOOM. Poetry.

If you read this, I am sorry. I hope you gleaned something from this very rough blast of writing.
