# Michael Bruce Design Site

A home for content relating to my professional work in software development and
digital UI/UX design.

# Architecture

server.go describes a web server, it serves a number of static files stored
under site/static. Articles are written in markdown and generated into pages as
the server is launched.

# Style guide

Headlines are in Montserrat Bold
Subtitles/Text is in Montserrat
