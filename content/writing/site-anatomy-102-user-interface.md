---
title: "Site Anatomy 102"
date: 2018-11-11T00:00:00+01:00
---
# Site Anatomy 102: User Interface

<div><img src="/images/site-anatomy-102.png"></div>

<span class="t-drop-cap">T</span><span>oday I'm going to be extending the site we started in [Site Anatomy 101](/2018-11-04-site-anatomy-101). In the last post, I created a function that could be called through the endpoint `/nutrient-composition-1` and what this would do is stream a graph to the web browser.</span>

You may want to see the updated site for this article before continuing,
[here](/nutrient-composition-2).

Websites are all about interactivity, some input would make a nice addition.
To do this we need to **take user input** from the web browser and **pass it
back to the server**, since the graph is generated there.

Ideally I'd like to accept updates to the user input and redisplay the nutrition
graph without having to reload the page every time. Fortunately, there is a
collection of techniques, known as **AJAX** that I will be using to asynchronously
communicate with my site server. The code I'm going to use for communicating
with the server is as follows:

```
function drawGraph() {
  var candidates = document.getElementById('candidates').value;

  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/base64-graph');
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onload = function() {
    if (xhr.status == 200) {
      console.log('success!');
      var graph = '<img src="data:image/png;base64,' + xhr.responseText + '" />'
      document.getElementById('graph-position').innerHTML = graph;
    } else {
      console.log('Request for graph failed, returned: ' + xhr.status);
    }
  }
  xhr.send(JSON.stringify({candidates: candidates}));
}
```

The function `drawGraph()` is a function that is fired from the web browser when
the page first loads & every time a keypress happens in the input field we will
be including. It's composed of a few components so let's break this down:

- `candidates` is a variable that holds the current value of a `#candidates`
  input field.
- `xhr` is a XMLHttpRequest, which is used to retrieve our updated graph. This
  is the backbone of AJAX programming.
- We are calling `open` on `xhr` to define the request to be made to the server,
  in this case it's a `POST` request to `/base64-graph`
- `setRequestHeader` is used to let the server know that in our request we will
  also be sending JSON, a common format to providing arguments in request like
  these.
- `onload` defines how to respond after receiving a message back from the
  server, upon success we are filling an area of the page marked with the
  element id `#graph-position` with the graph sent back from the server.
- `send` sends the request with the `candidates` argument back to the server.

This new function is the major change required, all that is needed are the HTML
elements that is interacts with `#candidates` for the input box, `#graph-position`
for displaying the graph and we're done on the frontend.

## On the server/backend

Here I made two changes:

- Since we are now placing our graph which is a PNG, inside a website; We now
   need to **encode this data in Base64**. The reason for this is that an image is
   stored as binary data and we need ensure that the HTML page does not
   interpret certain characters as part of it's language, corrupting the image.
- **Include extra parameters** to accept the new candidates parameter from the
  frontend. I did this by adding a new endpoint `/base64-graph` that decodes the
  JSON sent from the AJAX function previously described and then invokes the
  original `Graph` function with a new parameter for the candidates input.

## In Summary

AJAX is a powerful technique when building a site that needs to interact with
the backend server frequently, as is the case in this example. JSON is the
standard method of transferring information between the front and back sections
of a website.

## Thanks for reading

I'm looking forward to continuing this series further, I hope you enjoyed
reading this installment.

I take a lot of enjoyment pulling apart life’s complex problems, particularly
when it comes to technology. If you liked this article, perhaps you’d like my
help building your next site or system. You can get started by using the form
below.

Until next time!

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/2018-11-11-site-anatomy-102" />
<meta property="og:title" content="Site Anatomy 102: User Interface" />
<meta property="og:image" content="https://michaelbruce.co/images/site-anatomy-102.png" />
