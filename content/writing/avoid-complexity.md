---
title: The secret to avoid complexity that you see everyday.
date: 2019-10-26
publishdate: 2019-10-26
---

# The secret to avoid complexity that you see everyday.

<div><img src="/images/complexity-blur.jpg"></div>

Complexity is both the solution and the problem in software.


Your code must solve something meaningful. It exists because it has utility. Big problems can break down into easier to manage parts. When you represent all or part of something in a new way we call this abstraction.

When creating a budget, you might choose to separate expenses into the months that they occur. You could split spending by purpose, e.g rent, food, travel. This separation abstracts the your account transactions into something easier to understand.

By taking these grouped totals and plotting them, we can further abstract information. The result is a visual view, from which we can clearly see where higher spending occurs.

Through abstraction, we created a particular view of our data. This view is taken from a narrow angle, making it harder to help us answer different questions.

While it can be used to handle complexity, abstractions themselves create their own complexities. Particularly when we choose to build upon them.

Building on abstraction creates it's own complexity. Building on these simpler views may miss out on key information. When grouping spending transactions by food, we no longer know how much was spent on fast food.

When creating a new abstraction, be sure that your foundation gives you what is needed. You may need to go back to your original data.

It's common for abstractions to be built intentionally as a middle layer. Abstractions can be built upon, but they must be designed with this in mind.

A table or spreadsheet is perhaps the most common layering abstraction. Information is not naturally presented to us in tabular format. It is collected in this way because this design provides utility.

It also has an added bonus of being a familiar abstraction. Anyone who has used a computer knows what a table is. They will have a basic knowledge of what it can be used for.

Avoiding complexity is a mistake. Embrace complexity and you will find it easier to work with.

Naming is another way of abstracting complexity. We use the name PI to describe a neverending mathematical constant. Using the name in formula makes them easier to understand than writing 3.1415926535.. every time.

The same is true in software. You don't need to know all the actions to write a file, the implementation can be written as a function. From then on, we can simply refer to the name of that function.

Once you realise that *everything* in the coded language you write builds on a series of abstractions. Character Encoding, JSON parsing, Networking. You can really embrace the idea that complexity can be embraced.

A good layer of abstraction will allow you to forget entirely about it's implementation.

The next time you are faced with a complex problem, ask yourself if you are working at a useful layer of abstraction. Think about how your solution fits in. Embrace Complexity.

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@mikepjb" />
<meta name="twitter:creator" content="@mikepjb" />
<meta property="og:url"
content="https://michaelbruce.co/writing/avoiding-complexity/" />
<meta property="og:title" content="The secret to avoid complexity that you see everyday." />
<meta property="og:image" content="https://michaelbruce.co/images/complexity-blur.jpg" />
